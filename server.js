var config = require('config'),
	express = require('express'),
	mongoose = require('mongoose'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    routes = require('./routes'),
    listdb = require('./listdb'),
    models = require('./models');

var app = express();

app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.use(express.static(__dirname + '/public'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/', routes);
app.use('/db/', listdb);

console.log('Listening on port ' + config.port);
app.listen(config.port)