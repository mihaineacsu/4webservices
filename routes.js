var express = require('express'),
	models = require('./models'),
    router = express.Router(),
    fixtures = require('./fixtures');

var Users = models.user,
	Article = models.article,
	ArticleSection = models.articleSection,
	Action = models.action,
	Growth = models.growth;

router.get('/', function(req, res){
	res.render('index');
});

/*
 ** GetArticleoftheDay test client 
 */
router.get('/getDailyArticle', function(req, res){
	res.render('getDailyArticle');
});

/*
 ** WebService 1: GetArticleoftheDay
 */
 router.post('/article', function(req, res){
 	var extractFields = function(article){
 		return {
 			ArticleID: article.ArticleID,
 			Caption: article.Caption,
 			Description: article.Description,
 			ImageName: article.ImageName
 		};
 	};

	var body = req.body;
	var params = {
				Day: body.Day,
				PreviousArticleID: body.PreviousArticleID,
				}

	for (var key in params) {
		if (typeof params[key] === 'undefined')
			res.send('Missing params');
	}

	Article.findOne({'_id': params.PreviousArticleID}, function(err, previousArticle){
		if (err){
				console.log(err);
				res.status(500);
				return res.send(err);
		}

		if (!previousArticle){
			return res.send('No previous article with id ' + params.PreviousArticleID + ' found');
		}

		Article.find({'Day': {$gte: parseInt(previousArticle.Day) + 1}}).sort({_id: 1}).limit(1).exec(function(err, nextHigher){
			nextHigher = nextHigher[0];

			if (err){
				console.log(err);
				res.status(500);
				return res.send(err);
			}

			if (!nextHigher){
				if (previousArticle.Day == params.Day)
					return res.send('NULL');
				else if (previousArticle.Day < params.Day){
					previousArticle.Day = parseInt(previousArticle.Day) + 1;
					previousArticle.save(function(err){
						if (err){
							console.log(err);
							res.status(500);
							return res.send(err);
						}					
					
						return res.send(extractFields(previousArticle));
					});
				}
				else
					res.send('Err: previous article .day field is higher than the day searched for');
			}

			return res.send(extractFields(nextHigher));
		});
	});
});


/*
 ** GetArticleDetails test client 
 */
 router.get('/getArticleByID', function(req, res){
 	res.render('getArticleByID');
 });

/*
 ** WebService 2: GetArticleDetails
 */
router.get('/article/:ArticleID', function(req, res){
	ArticleID = req.params.ArticleID;
	Article.findOne({'_id': ArticleID}, function(err, article){
		if (err){
			console.log(err);
			res.status(500);
			return res.send(err);
		}

		if (!article){
			return res.send('ArticleID not found');
		}

		Action.find({}).populate({path: 'SectionID', match: {ArticleID: ArticleID}}).exec(function(err, actions){
			if (err){
				return console.log(err);
			}

			var str = '<xmp><Article>\n\t<ArticleInformation>' + article.ArticleID + ', ' + article.Caption +
					  ', ' + article.Description + ', ' + article.ImageName + '\n';
			var sectionsDict = {};

			if (actions)
				actions.forEach(function (action, index) {
					if (!action.SectionID)
						return;

					var SectionIndex = action.SectionID._id;

					if (typeof sectionsDict[SectionIndex] === 'undefined')
						sectionsDict[SectionIndex] = {section: action.SectionID, actions: [action]};
					else
						sectionsDict[SectionIndex].actions.push(action);
				});

			ArticleSection.find({'ArticleID': ArticleID}, function(err, sections){
				if (sections)
					sections.forEach(function (section, index) {
						var SectionIndex = section._id;

						if (typeof sectionsDict[SectionIndex] === 'undefined')
							sectionsDict[SectionIndex] = {section: section, actions: []};
					})

				var keys = Object.keys(sectionsDict);
				if (keys)
					keys.forEach(function (key, index) {
						var section = sectionsDict[key].section;
						str += '\t\t<SectionInformation>' + section.SectionNumber + ', ' + section.Caption + ', ' + 
						section.SectionDescription + ', ' + section.ImageName + '\n';
						var nestedActions = sectionsDict[key].actions;

						nestedActions.forEach(function (action, index) {
							str += '\t\t\t<Actions>' + action._id + ', ' + action.ActionDescription + '</Actions>\n';
						});

						str += '\t\t</SectionInformation>\n';
					});

				str += '\t</ArticleInformation>\n</Article></xmp>';
				
				res.send(str);
			});
		});
	});
});

/*
 ** InsertIntoGrowth test client 
 */
 router.get('/insertGrowth', function(req, res){
	res.render('insertGrowth');
});

/*
 ** WebService 3: InsertIntoGrowth
 */
router.post('/growth', function(req, res){
	var body = req.body;

	var params = {
				UserID: body.UserID,
				Date: body.Date,
				Height: body.Height,
				Weight: body.Weight,
				Head: body.Head
				}

	for (var key in params) {
		if (typeof params[key] === 'undefined')
			console.log('Missing params');	
	}

	Growth.create(params, function(err, newGrowthEntry){
		if (err){
			console.log(err);
			res.status(500);
			res.send(err);
		}

		console.log('Growth entry saved');
		res.redirect(200, '/getGrowth');
	})
});


/*
 ** GetGrowthValues test client 
 */
 router.get('/getGrowth', function(req, res){
	res.render('getGrowth');
});

/*
 ** WebService 4: GetGrowthValues
 */
router.get('/growth/:UserID', function(req, res){
	Growth.find({'UserID': req.params.UserID}, function(err, entries){
		if (err){
			console.log(err);
			res.status(500);
			res.send(err);
		}

		if (!entries){
			return res.send('No entries found');
		}

		// res.send({'Date': growthEntry.Date,
		// 		  'Height': growthEntry.Height,
		// 		  'Weight': growthEntry.Weight,
		// 		  'Head': growthEntry.Head});
		res.send(entries);
	});
});


router.get('/fixtures', function(req, res){
	fixtures(res);

	res.send('Fixtures done, db populated');
});

module.exports = router;
