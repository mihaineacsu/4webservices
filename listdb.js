var express = require('express'),
	models = require('./models'),
    router = express.Router();

var Users = models.user,
	Article = models.article,
	ArticleSection = models.articleSection,
	Action = models.action,
	Growth = models.growth;

router.get('/users', function(req, res){
	Users.find({}, function(err, entries){
		res.send(entries);
	});
});	

router.get('/articles', function(req, res){
	Article.find({}, function(err, entries){
		res.send(entries);
	});
});	

router.get('/articlesections', function(req, res){
	ArticleSection.find({}, function(err, entries){
		res.send(entries);
	});
});

router.get('/actions', function(req, res){
	Action.find({}, function(err, entries){
		res.send(entries);
	});
});

router.get('/growths', function(req, res){
	Growth.find({}, function(err, entries){
		res.send(entries);
	});
});

module.exports = router;
