var	models = require('./models');

var User = models.user,
	Article = models.article,
	ArticleSection = models.articleSection,
	Action = models.action,
	Growth = models.growth;

module.exports = function(){
	User.remove({}, function(err) {
		console.log('collection removed') 
		User.resetCount(function(){
			User.create({UserEmail: 'mihai.mneacsu@gmail.com', UserName: 'Mihai'}, function(err, newUser){
				if (err){
					return console.log(err);
				}
			});

			User.create({UserEmail: 'dummymail@yahoo.com', UserName: 'dummyname'}, function(err, newUser){
				if (err){
					return console.log(err);
				}
			});

			User.create({UserEmail: 'Steve@microsoft.com', UserName: 'Steve'}, function(err, newUser){
				if (err){
					return console.log(err);
				}
			});
		});
	});

	Article.remove({}, function(err) { 
		console.log('collection removed') 
		Article.resetCount(function(){
			Article.create({_id: 1, Day: 1, Caption: '5 Best Destinations of the world',
						   Description: 'Last updated on November 5 in Travel', Active: true, 
						   ImageName: 'Toptraveldestinations.jpg'}, function(err, newArticle){
				if (err){
					return console.log(err);
				}
			});


			Article.create({_id: 4, Day: 10, Caption: '5 Best Destinations of the world',
						   Description: 'Last updated on November 5 in Travel', Active: true, 
						   ImageName: 'Toptraveldestinations.jpg'}, function(err, newArticle){
				if (err){
					return console.log(err);
				}
			});

			Article.create({_id: 2, Day: 2, Caption: '10 Best-Selling Products Of All Time',
						   Description: 'By Silicon India', Active: true, 
						   ImageName: 'TopProducts.jpeg'}, function(err, newArticle){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			Article.create({_id: 3, Day: 2, Caption: '35 fantastic free Android games',
						   Description: 'Fun doesn’t have to require funding. Got an Android phone or tablet? Then these are the greatest games… gratis!', Active: true, 
						   ImageName: 'Andriod Games'}, function(err, newArticle){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});
		});
	});

	ArticleSection.remove({}, function(err) { 
		console.log('collection removed') 
		ArticleSection.resetCount(function(){
			ArticleSection.create({SectionID: 1, ArticleID: 1, SectionNumber: 1, Caption: 'Cape Town, South Africa',
							SectionDescription: 'The most southern point of Africa where you can see whales and dolphins, go to the top of Table Mountain and see spectacular views or enjoy a visit to one of the many vineyards and taste some of the best wines produced in the world today. Robben Island, the historic prison where Nelson Mandela was held, is a ‘must do, for any visitor.', 
							Active: true, ImageName: 'Capetown1.jpg'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			ArticleSection.create({SectionID: 2, ArticleID: 1, SectionNumber: 2, Caption: 'Sydney Australia',
							SectionDescription: 'Be a beach bum in the morning, tourist in the afternoon and culture vulture in the evening – in Sydney you can be whatever you want to be and still manage to blend in. This city is often the first port of call for visitors and it rolls out a brilliant welcome mat. Sydney Opera House, Manly Beach and the Blue Mountains are just a snippet of the list of things to do here.', 
							Active: true, ImageName: 'Sydney.jpg'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			ArticleSection.create({SectionID: 3, ArticleID: 1, SectionNumber: 3, Caption: 'Machu Picchu, Peru',
							SectionDescription: 'This destination offers access to what remains of one of the most fascinating extinct cultures and to the shrinking Amazonian jungle. The local people are proud to share their heritage with visitors and to guide those that are interested through the more remote areas to scenery that few places can compete with.', 
							Active: true, ImageName: 'NULL'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			ArticleSection.create({SectionID: 4, ArticleID: 2, SectionNumber: 1, Caption: 'PlayStation',
							SectionDescription: 'PlayStation that was ranked at the tenth position on the list is a series of video game consoles created and developed by Sony Computer Entertainment. The brand, which was first introduced in 1994 in Japan, has dominated the fifth, sixth, seventh and eighth generations of video gaming.', 
							Active: true, ImageName: 'Paystation.jpg'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			ArticleSection.create({SectionID: 5, ArticleID: 2, SectionNumber: 2, Caption: 'Lipitor',
							SectionDescription: 'Lipitor, which came ninth on the list of 10 Best-selling Products of All Time compiled by 247wallst.com, is a trade name of a calcium salt (Atorvastatin) marketed by Pfizer. This drug, which is a member of the drug class known as statins, is used for lowering the level of LDL—the so called bad cholesterol—in the blood. The drug also stabilizes plaque and prevents strokes through anti-inflammatory and other mechanisms.', 
							Active: true, ImageName: 'Lipitor.jpg'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			ArticleSection.create({SectionID: 6, ArticleID: 3, SectionNumber: 1, Caption: 'BADLAND',
							SectionDescription: 'Quite beautiful, is Badland. It’s a physics-based auto-scrolling game where you ‘push’ a bat-like creature through a series of atmospheric, silhouetted levels (and yeah, before you say it, we know its look is “heavily inspired” by Limbo).', 
							Active: true, ImageName: 'NULL'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			ArticleSection.create({SectionID: 7, ArticleID: 3, SectionNumber: 2, Caption: 'AIRBORNE',
							SectionDescription: "iven that this is the eighth title in the Asphalt series, it probably comes as no surprise Gameloft's got a bit bored having sports cars merely zoom along at breakneck speeds and drift for ludicrous distances.", 
							Active: true, ImageName: 'Airborne.jpg'}, function(err, newArticleSection){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});
		});
	});

	Action.remove({}, function(err) { 
		console.log('collection removed') 
		Action.resetCount(function(){
			Action.create({ActionID: 1, SectionID: 1, ActionDescription: 'Action1', Active: true}, function(err, newAction){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			Action.create({ActionID: 2, SectionID: 1, ActionDescription: 'Action2', Active: true}, function(err, newAction){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});

			Action.create({ActionID: 3, SectionID: 1, ActionDescription: 'Action3', Active: true}, function(err, newAction){
				if (err){
					return console.log(err);
				}

				console.log('saved');
			});
		});
	});

	Growth.remove({}, function(err) { 
		console.log('collection removed') 
		Growth.resetCount(function(){});
	});
};