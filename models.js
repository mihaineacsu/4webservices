var mongoose = require('mongoose'),
	autoIncrement = require('mongoose-auto-increment'),
	validate = require('mongoose-validator'),
	config = require('config');

var connection = mongoose.createConnection(config.db);

autoIncrement.initialize(connection);

var Schema = mongoose.Schema;

stringValidatorFactory = function(name, maxLength){
	return [
		validate({
		validator: 'isLength',
		arguments: [3, maxLength],
		message: name +  ' should be between 3 and ' + maxLength + ' characters'
		})
	];
};

/*
 ** User schema
 */
var userSchema = new Schema(
	{
		_id: Number,
		UserEmail: {type: String, required: true, validate: stringValidatorFactory('UserEmail', 200)},
		UserName: {type: String, required: true, validate: stringValidatorFactory('UserName', 200)}
	},
	{
		collection: 'Users'
	}
);
userSchema.plugin(autoIncrement.plugin, {model: 'User', field: '_id'});
userSchema.virtual('UserID').get(function() {return this._id;});


/*
 ** Article schema
 */
var articleSchema = new Schema(
	{
		_id: Number,
		Day: {type: Number, required: true},
		Caption: {type: String, required: true, validate: stringValidatorFactory('Caption', 200)},
		Description: {type: String, required: true, validate: stringValidatorFactory('Description', 200)},
		Active: {type: Boolean, required: true},
		ImageName: {type: String, required: true, validate: stringValidatorFactory('ImageName', 100)}
	},
	{
		collection: 'Articles'
	}
);
articleSchema.plugin(autoIncrement.plugin, {model: 'Article', field: '_id'});
articleSchema.virtual('ArticleID').get(function() {return this._id;});


/*
 ** Article Section schema
 */
var articleSectionSchema = new Schema(
	{
		_id: Number,
		ArticleID: {type: Number, required: true, ref: 'Article'}, Caption: {type: String, required: true, validate: stringValidatorFactory('Caption', 200)},
		SectionNumber: {type: Number, required: true}, ImageName: {type: String, required: true, validate: stringValidatorFactory('ImageName', 100)},
		SectionDescription: {type: String, required: true, validate: stringValidatorFactory('SectionDescription', 2000)}
	},
	{
		collection: 'ArticleSections'
	}
);
articleSectionSchema.plugin(autoIncrement.plugin, {model: 'ArticleSection', field: '_id'});
articleSectionSchema.virtual('SectionID').get(function() {return this._id;});


/*
 ** Action schema
 */
var actionSchema = new Schema(
	{
		_id: Number,
		SectionID: {type: Number, required: true, ref: 'ArticleSection'},
		ActionDescription: {type: String, required: true, validate: stringValidatorFactory('ActionDescription', 2000)},
		Active: {type: Boolean, required: true}
	},
	{
		collection: 'Actions'
	}
);
actionSchema.plugin(autoIncrement.plugin, {model: 'Action', field: '_id'});
actionSchema.virtual('ActionID').get(function() {return this._id;});


/*
 ** Growth schema
 */
var growthSchema = new Schema(
	{
		_id: Number,
		UserID: {type: Number, required: true, ref: 'User'},
		Date: {type: Date, required: true},
		Height: {type: Number, required: true},
		Weight: {type: Number, required: true},
		Head: {type: Number, required: true}
	},
	{
		collection: 'Growths'
	}
);
growthSchema.plugin(autoIncrement.plugin, {model: 'Growth', field: '_id'});
growthSchema.virtual('GrowthID').get(function() {return this._id;});


module.exports = {
				 user: connection.model('User', userSchema),
				 article: connection.model('Article', articleSchema),
				 articleSection: connection.model('ArticleSection', articleSectionSchema),
				 action: connection.model('Action', actionSchema),
				 growth: connection.model('Growth', growthSchema)
				};
